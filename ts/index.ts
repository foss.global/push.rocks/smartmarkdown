import * as plugins from './smartmarkdown.plugins.js';
import { MdParsedResult } from './smartmarkdown.classes.mdparsedresult.js';

export class SmartMarkdown {
  public static async easyMarkdownToHtml(mdStringArg: string) {
    const smartmarkdownInstance = new SmartMarkdown();
    const mdParsedResult = await smartmarkdownInstance.getMdParsedResultFromMarkdown(mdStringArg);
    return mdParsedResult.html;
  }

  constructor() {}

  /**
   * create a MdParsedResult from markdown
   * @param mdStringArg
   */
  public async getMdParsedResultFromMarkdown(mdStringArg: string): Promise<MdParsedResult> {
    const result = await MdParsedResult.createFromMarkdownString(mdStringArg);
    return result;
  }

  public htmlToMarkdown(htmlString: string): string {
    const turndownInstance = new plugins.turndown({
      headingStyle: 'atx',
      codeBlockStyle: 'fenced',
    });
    turndownInstance.use(plugins.turndownPluginGfm.gfm);
    return turndownInstance.turndown(htmlString);
  }
}
